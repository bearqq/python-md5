# coding: utf-8
#15-09-27
#some definitions:
Din='num.txt'

#imports
import hashlib,codecs

#functions
fin=codecs.open(Din,'r','gbk')

def md5it(origstr,salt=''):
    if salt:
        return hashlib.md5(''.join((origstr.encode('utf-8'),salt))).hexdigest()
    else:
        return hashlib.md5(origstr.encode('utf-8')).hexdigest()

def searchmd5(md,salt=''):
    i=10000
    j=0
    while 1:
        ps=fin.readline()
        if ps:
						ps=ps.strip()
						if salt:
								m1=md5it(ps,salt)
								m2=md5it(md5it(ps),salt)
						else:
								m1=md5it(ps)
								m2=md5it(m1)
						if (m1==md) or (m2==md):
								print j,(10000-i)
								return ps
        else:
            return
        i=i-1
        if not i:
            i=10000
            j=j+1
            print j,'0000'


def main():
    md=raw_input('input a md5 to test\n')
    salt=raw_input('ant salt?\n')
    print searchmd5(md.strip(),salt.strip())
    raw_input('Finish')

if __name__ == '__main__':
	main()

fin.close()