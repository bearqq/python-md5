# coding: utf-8
#15-09-27
#some definitions:


#imports
import hashlib


#functions

def md5it(origstr,salt=''):
    if salt:
        return hashlib.md5(''.join((origstr,salt))).hexdigest()
    else:
        return hashlib.md5(origstr.encode('gbk')).hexdigest()

print md5it('111111')
print md5it(md5it('111111'))
print md5it(md5it('111111'),'somesalt')	#forum salt
raw_input('Done!')