# coding: utf-8
#15-09-27
#some definitions:
Din='num.txt'
#Dout='md5_num.txt'
Dbout='db_num.db'

#imports
import hashlib,codecs
import sqlite3

#functions
def md5it(origstr,salt=''):
    if salt:
        return hashlib.md5(''.join((salt,origstr.encode('utf-8')))).hexdigest()
    else:
        return hashlib.md5(origstr.encode('utf-8')).hexdigest()

def main():
    conn = sqlite3.connect(Dbout)
    c = conn.cursor()
    fin=codecs.open(Din,'r','gbk')
    #fout=codecs.open(Dout,'a','utf-8')
    
    c.execute("CREATE TABLE num (pass text, md5_1 text, md5_2 text)")
    
    lines=fin.readlines()
    lines={}.fromkeys(lines).keys()
    
    i=10000
    
    for line in lines:
        m=line.strip()
        m1=md5it(m)
        #fout.write("%s,%s,%s\n" % (m,m1,md5it(m1)))
        c.execute("INSERT INTO num VALUES (?,?,?)",(m,m1,md5it(m1)))
        
        #print m
        i=i-1
        if i:
            continue
        else:
            print m
            i=10000
    
    fin.close()
    #fout.close()
    conn.commit()
    conn.close()
    
if __name__ == '__main__':
	main()
