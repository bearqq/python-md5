# coding: utf-8
#15-09-27
#some definitions:
Dbout='db_num.db'

#imports
import sqlite3
from bottle import route, run, template, Bottle

#log
from requestlogger import WSGILogger, ApacheFormatter
from logging.handlers import TimedRotatingFileHandler

#functions
app = Bottle()

def searchmd5(md):
    conn = sqlite3.connect(Dbout)
    c = conn.cursor()
    c.execute("SELECT * FROM num WHERE (md5_1 = ?) OR (md5_2 = ?)",(md,md,))
    r=c.fetchall()
    conn.close()
    return r

@app.route('/robots.txt')
def robots():
    return """\
User-agent: *
Disallow: /
Disallow: /*"""

@app.route('/')
def index():
    return "This is an 404 page and you don't know why."

@app.route('/md5/<md>')
def getmd5(md):
    return str(searchmd5(md))

def main():
    md=raw_input('input a md5 to test\n')
    print searchmd5(md)
    raw_input('Finish')

#log
handlers = [ TimedRotatingFileHandler('access.log', 'd', 7) , ]
app = WSGILogger(app, handlers, ApacheFormatter())


if __name__ == '__main__':
	#main()
	#run(host='0.0.0.0', port=80, server='paste')
	run(app, host='0.0.0.0', port=80)